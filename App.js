import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  TouchableOpacity,
  Keyboard,
  Platform,
  AsyncStorage
} from "react-native";
import AppHeader from "./components/AppHeader";
import AppFormPlaces from "./components/AppFormPlaces";
import AppPlacesList from "./components/AppPlacesList";
import Loader from "./components/Loader";

export default function App() {
  const isAndroid = Platform.OS == "android";
  const [viewPadding, setViewPadding] = useState(10);
  const [places, setPlaces] = useState([]);
  const [isPlaces, setIsPlaces] = useState(true);
  const [isLoaded, setIsLoaded] = useState(true);

  useEffect(() => {
    Keyboard.addListener(
      isAndroid ? "keyboardDidShow" : "keyboardWillShow",
      e =>
        setViewPadding({ viewPadding: e.endCoordinates.height + viewPadding })
    );

    Keyboard.addListener(
      isAndroid ? "keyboardDidHide" : "keyboardWillHide",
      () => setViewPadding({ viewPadding: viewPadding })
    );
    getItemStorage();
  });

  const getItemStorage = async () => {
    if (await AsyncStorage.getItem("PLACES")) {
      setPlaces(JSON.parse(await AsyncStorage.getItem("PLACES")));
    }
  };

  return (
    <KeyboardAvoidingView
      behavior="padding"
      style={[styles.container, { paddingBottom: viewPadding }]}
    >
      <View style={styles.head}>
        <AppHeader />

        <View style={styles.btnHeadContainer}>
          <TouchableOpacity
            style={styles.btnHead}
            onPress={() => setIsPlaces(true)}
          >
            <Text style={styles.color1}>Lieux</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btnHead}
            onPress={() => setIsPlaces(false)}
          >
            <Text style={styles.color1}>Ajouter un lieu</Text>
          </TouchableOpacity>
        </View>
      </View>
      {!isLoaded ? (
        <Loader />
      ) : (
        <View style={styles.width}>
          {isPlaces ? (
            <AppPlacesList data={places} />
          ) : (
            <AppFormPlaces places={places} addPlace={setPlaces} />
          )}
        </View>
      )}
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },
  head: {
    position: "absolute",
    top: 0,
    width: "100%"
  },
  width: {
    width: "100%"
  },
  btnHeadContainer: {
    flexDirection: "row",
    width: "100%"
  },
  btnHead: {
    alignItems: "center",
    justifyContent: "center",
    width: "50%",
    height: 40,
    backgroundColor: "#20232a"
  },
  bold: {
    fontWeight: "bold"
  },
  pt5: {
    paddingTop: 10
  },
  color1: {
    color: "#61dafb"
  },
  color2: {
    color: "#20232a"
  }
});
