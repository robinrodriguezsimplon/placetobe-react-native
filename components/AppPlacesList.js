import React from "react";
import { FlatList } from "react-native";
import { StyleSheet, View, Text } from "react-native";

export default function AppPlacesList({ data }) {
  return (
    <View style={styles.headerContainer}>
      <Text style={[styles.color2, styles.bold, styles.pt5]}>
        Voiçi la liste des lieux que j'ai pu visiter:
      </Text>
      {data.length !== 0 ? (
        <FlatList
          data={data}
          renderItem={({ item, index }) => (
            <View key={index}>
              <View style={styles.item}>
                <Text>{`- ` + item.name}</Text>
              </View>
              <View style={styles.hr} />
            </View>
          )}
        />
      ) : (
        <View style={styles.item}>
          <Text>Bah j'ai encore rien visité, je suis confiné !</Text>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    minHeight: "60%",
    backgroundColor: "white",
    paddingLeft: "10%",
    paddingRight: "10%"
  },
  header: {
    color: "#61dafb",
    fontSize: 20
  },
  bold: {
    fontWeight: "bold"
  },
  pt5: {
    paddingTop: 10
  },
  color1: {
    color: "#61dafb"
  },
  color2: {
    color: "#20232a"
  }
});
