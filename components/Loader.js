import React from "react";
import { ActivityIndicator, StyleSheet, View } from "react-native";

export default function Loader() {
  return (
    <View style={[styles.container, styles.horizontal]}>
      <ActivityIndicator size="large" color="#61dafb" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#000000e8",
    position: "absolute",
    zIndex: 1,
    top: "0",
    bottom: "0",
    left: "0",
    right: "0",
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  }
});
