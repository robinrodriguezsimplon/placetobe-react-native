import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Button,
  TextInput,
  Text,
  Picker,
  AsyncStorage
} from "react-native";

export default function AppFormPlaces({ places, addPlace }) {
  const [selectedValue, setSelectedValue] = useState("0");
  const [errorSelectedValue, setErrorSelectedValue] = useState(false);
  const [name, setName] = useState("");
  const [errorName, setErrorName] = useState(false);
  const [description, setDescription] = useState("");
  const [errorDescription, setErrorDescription] = useState(false);
  const [address, setAddress] = useState("");
  const [errorAddress, setErrorAddress] = useState(false);

  const handlePress = () => {
    if (selectedValue === "0") {
      setErrorSelectedValue(true);
    } else {
      setErrorSelectedValue(false);
    }
    if (!name.length) {
      setErrorName(true);
    } else {
      setErrorName(false);
    }
    if (!description.length) {
      setErrorDescription(true);
    } else {
      setErrorDescription(false);
    }
    if (!address.length) {
      setErrorAddress(true);
    } else {
      setErrorAddress(false);
    }
    if (errorSelectedValue || errorName || errorDescription || errorAddress) {
      return;
    } else {
      savePlace();
    }
  };

  const savePlace = async () => {
    addPlace(currentPlaces => [
      ...currentPlaces,
      {
        category: selectedValue,
        name: name,
        description: description,
        address: address,
        note: [],
        comments: []
      }
    ]);
    await AsyncStorage.setItem("PLACES", JSON.stringify(places));
  };

  return (
    <View style={styles.headerContainer}>
      <Picker
        selectedValue={selectedValue}
        style={errorSelectedValue ? styles.erreur : styles.color2}
        onValueChange={itemValue => {
          setSelectedValue(itemValue);
          setErrorSelectedValue(false);
          console.log(itemValue);
        }}
      >
        <Picker.Item label="Sélectionner une catégorie*" value="0" />
        <Picker.Item label="Bar" value="Bar" />
        <Picker.Item label="Restaurant" value="Restaurant" />
        <Picker.Item label="Hotel" value="Restaurant" />
        <Picker.Item label="Club" value="Restaurant" />
      </Picker>
      <Text style={errorName ? styles.erreur : styles.color2}>Nom*</Text>
      <TextInput
        value={name}
        onChangeText={name => {
          setErrorName(false);
          setName(name);
        }}
        placeholder="Ajouter un nom"
        style={styles.input}
      />
      <Text style={errorDescription ? styles.erreur : styles.color2}>
        Description*
      </Text>
      <TextInput
        value={description}
        onChangeText={description => {
          setErrorDescription(false);
          setDescription(description);
        }}
        placeholder="Ajouter une description"
        style={styles.input}
      />
      <Text style={errorAddress ? styles.erreur : styles.color2}>Adresse*</Text>
      <TextInput
        value={address}
        onChangeText={address => {
          setErrorAddress(false);
          setAddress(address);
        }}
        placeholder="Ajouter une adresse"
        style={styles.input}
      />
      <Button onPress={handlePress} color="#61dafb" title="Ajouter" />
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "white",
    paddingLeft: "10%",
    paddingRight: "10%",
    paddingTop: 20
  },
  header: {
    color: "#61dafb",
    fontSize: 20
  },
  input: {
    color: "white",
    backgroundColor: "#282c34",
    padding: 5,
    marginBottom: 10
  },
  bold: {
    fontWeight: "bold"
  },
  color1: {
    color: "#61dafb"
  },
  color2: {
    color: "#20232a"
  },
  erreur: {
    color: "red"
  }
});
