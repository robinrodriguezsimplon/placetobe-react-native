import React from "react";
import { StyleSheet, View, Text } from "react-native";

export default function AppHeader() {
  return (
    <View style={styles.headerContainer}>
      <Text style={styles.header}>
        Les meilleurs endroits à visiter j'ai juré
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: "#20232a",
    flexDirection: "row",
    paddingTop: 50,
    justifyContent: "center",
    height: 100,
    alignItems: "center"
  },
  header: {
    color: "#61dafb",
    fontSize: 20
  }
});
